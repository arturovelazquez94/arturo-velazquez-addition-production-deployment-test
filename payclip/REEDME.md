# Payclip assesment on production

This guide includes the step by step statements to create a kubernetes cluster in aws and payclip microservices deployment.


## Prerequisites
```python
AWS CLI Installed
AWS credentials correctly configured (nano .aws/credentials)
kubectl installed
eksctl installed
```

## Set up and configuration

Set up and configuration need to be done one time, each time there is the need to create a
kubernetes cluster, mongodb replicaset and first time deployment.

### Cluster creation EKS

>eksctl create cluster --name=prod-assesment-payclip --region=us-west-2 --node-type=t2.medium --nodes=3

Creation time takes 10-20 minutes approximately

### Grant kubernetes cluster permissions (Important step)

>kubectl create clusterrolebinding admin --clusterrole=cluster-admin --serviceaccount=default:default

### Mongodb 

### Create storage class

The provisioner is using “aws-ebs” which is Elastic Block Storage (EBS)in AWS

>kubectl apply -f mongodb-storage-class.yaml

Make it as default

>kubectl patch storageclass sc-gp2 -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'

### Grant the default service account view permissions

>kubectl apply -f mongodb-permissions.yaml

### Create statefulset

>kubectl apply -f mongodb-statefulset.yaml
>kubectl get sts

Creates a replicaset in each of the availability zones, each one with an EBS persistentVolumeClaim of 5 GB (It can be configured)

#### Verify the replicaset configuration

>kubectl exec -ti mongo-0 mongo

>rs.status();

#### Enable secondaries write operations

>kubectl exec -ti mongo-1 mongo

>rs.slaveOk();

>kubectl exec -ti mongo-2 mongo

>rs.slaveOk();

In order to understand what replication is go to the documentation
https://docs.mongodb.com/manual/replication/

#### Scaling the replicaset(If necessary)

kubectl scale --replicas=7 statefulset mongo

#### Expose mongo service as load balancer to be able to acces it

>kubectl apply -f mongodb-service-mongo-0.yaml
>kubectl get svc

### ConfigMaps creation (Properties in kubernetes)

We have to create the environment variable $CONFIG_SERVER_PATH with the path of the config server repository

We have to create one configmap time for each of the microservices (On creation time)

>kubectl create configmap transactions --from-file=/home/arturo/Desktop/GIT/PAYCLIP/payclip-config-properties/production/transactions/application.yml
>kubectl create configmap users --from-file=/home/arturo/Desktop/GIT/PAYCLIP/payclip-config-properties/production/users/application.yml
>kubectl create configmap gateway --from-file=/home/arturo/Desktop/GIT/PAYCLIP/payclip-config-properties/production/gateway/application.yml

### Configmap inspection
>kubectl get configmap configmap_name -o yaml
>kubectl describe configmap configmap_name

### Configmaps updates

>kubectl create configmap configmap_name --from-file=$PATH-CONFIG-SERVER/microservice_id/application.yml -o yaml --dry-run | kubectl replace -f-

e.g. 
>kubectl create configmap transactions --from-file=$CONFIG_SERVER_PATH/transactions/application.yml -o yaml --dry-run | kubectl replace -f-  

## Microservices deployment

>kubectl apply -f transactions-deployment.yaml,users-deployment.yaml,gateway-deployment.yaml

### Pod inspection

>kubectl get pods
>kubectl describe pod pod_name

### Services inspection

>kubectl get svc
>kubectl describe svc service_name


### Statefulset inspection

>kubectl get sts
>kubectl describe sts statefulset_name

### (The first time and in case there is the need of changing the gateway service) 
>kubectl apply -f gateway-service-loadbalancer.yaml

## License
[MIT](https://choosealicense.com/licenses/mit/)
